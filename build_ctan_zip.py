#!/usr/bin/python3

"""Create zip file for download to CTAN"""

import os
import subprocess

#
# A simple script to package pkgcheck for CTAN
#

BASE = 'pkgcheck'
PKGCHECK_ZIP = BASE + '/pkgcheck.zip'

#
# file list with intended permission
#
FILE_LIST = {
    "": 0o755,
    "README.md": 0o644,
    "quick_intro.txt": 0o644,
    "CHANGES.md": 0o644,
    "LICENSE-APACHE": 0o644,
    "LICENSE-MIT": 0o644,

    "bin": 0o755,
    "bin/pkgcheck": 0o755,

    "Cargo.toml": 0o644,

    "src": 0o755,
    "src/filemagic.rs": 0o664,
    "src/generate.pest": 0o664,
    "src/generate.pest.md": 0o664,
    "src/gparser.rs": 0o664,
    "src/linkcheck.rs": 0o664,
    "src/main.rs": 0o664,
    "src/messages": 0o775,
    "src/messages/errorsd.rs": 0o664,
    "src/messages/fatald.rs": 0o664,
    "src/messages/informationd.rs": 0o664,
    "src/messages/mod.rs": 0o664,
    "src/messages/warningsd.rs": 0o664,
    "src/recode.rs": 0o664,
    "src/utils.rs": 0o664,

    "docs": 0o755,
    "docs/fatald.tex": 0o644,
    "docs/errorsd.tex": 0o644,
    "docs/informationd.tex": 0o644,
    "docs/pkgcheck.tex": 0o644,
    "docs/title.tex": 0o644,
    "docs/warningsd.tex": 0o644,
    "docs/pkgcheck.pdf": 0o644,

        }

def main():
    """Create the zip file in the directory above the project directory"""

    file_list = {BASE + '/' + k: v  for (k, v) in FILE_LIST.items()}


    # change directory to one up
    os.chdir('..')


    # first unlink the zip file
    if os.path.exists(PKGCHECK_ZIP):
        os.unlink(PKGCHECK_ZIP)


    # set permissions accordingly
    for file, perm in file_list.items():
        #printf("chmod %#o $f\n", $p);
        os.chmod(file, perm)

    files = list(file_list.keys())

    print("Zipping the files for the CTAN package")

    cmd = ['/usr/bin/zip', PKGCHECK_ZIP] + files

    subprocess.call(cmd)

main()
