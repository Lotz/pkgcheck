.PHONY: DEBUG RELEASE CLIPPY MUSLRUST-LOCAL STATIC 

DEBUG:
	cd docs; make -s RUST_FILES
	cargo build

TEST:
	cargo test --tests

RELEASE:
	cd docs; make -s RUST_FILES
	cargo build --release


CLIPPY:
	rustup run nightly cargo clippy

DOC:
	cd docs; make pkgcheck.pdf

MUSLRUST:
	docker pull clux/muslrust

MUSLRUST-LOCAL:
	docker build -t mlotz/muslrust-local .

STATIC:
	sudo docker run -v cargo-cache:/root/.cargo/registry:Z -v "${PWD}:/volume:Z" --rm -it clux/muslrust:stable cargo build --release

DEB:
	cargo deb --no-build

RPM:
	cargo rpm init
	cargo rpm build

bin/pkgcheck: target/x86_64-unknown-linux-musl/release/pkgcheck 
	install -s target/x86_64-unknown-linux-musl/release/pkgcheck bin/
	strip bin/pkgcheck

UPLOAD_TO_COMEDY:
	make bin/pkgcheck
	scp bin/pkgcheck manfred@comedy.dante.de:~/bin/pkgcheck

LOCAL_INSTALL:
	install -s bin/pkgcheck /dante/ctan/bin/

CTAN_ZIP:
	make DOC
	make RELEASE
	make STATIC
	make bin/pkgcheck
	./build_ctan_zip.p6

Z:
	cd ..; zip pkgcheck-ctan.zip pkgcheck.rs/bin/pkgcheck pkgcheck.rs/docs/*.tex pkgcheck.rs/docs/pkgcheck.pdf pkgcheck.rs/README.md

