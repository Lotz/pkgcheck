#!/usr/bin/env python3
"""
This script is used to upload a package to CTAN.
It uses curl to send the POST request.

Without any parameter it does just a sort of validation
of the package to be uploaded

CAUTION: The validation is not as thorough as what `pkgcheck` does

If `--upload` is specified the package gets uploaded to CTAN.

If required it could be enhanced to be a generic script.


When an upload succeeded there should be
- an HTTP status 200
- a message saying
    [["INFO","Upload succeeded"]]%
"""


import subprocess
import argparse
import sys
from typing import List, Dict, Any, MutableMapping

import rtoml as toml
import requests


def add_requests_parm_from_toml(
    conf: MutableMapping[str, Any], parm: str, cmd: Dict[str, Any]
) -> None:
    """
    Adds parameters for the requests post call

    - first get value from  toml file or None in case
      the `parm` is not specified
    - if the value gotten is a list add the values as a tuple
    - if the value gotten is just a simple value add the value as such
    """
    val = conf.get(parm)
    if val:
        if isinstance(val, list):
            # create tuple from list
            tval = tuple(val)
            cmd[parm] = tval
        else:
            cmd[parm] = val


def add_curl_parm_from_toml(
    conf: MutableMapping[str, Any], parm: str, cmd: List[str]
) -> None:
    """
    Adds command line parameter(s) for the curl command

    - first get value from  toml file or None in case
      the `parm` is not specified
    - if the value gotten is a list add command line args for each
      iten in the list
    - if the value gotten is just a simple value add command line
      arg for that value
    """

    val = conf.get(parm)
    if val:
        if isinstance(val, list):
            for v in val:
                cmd.append('-F')
                cmd.append(parm + '=' + v)
        else:
            cmd.append('-F')
            cmd.append(parm + '=' + val)


def main() -> None:
    """
    Parse command line args and invoke curl to either
    - validate an upload, or
    - to upload a package to CTAN
    """

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--upload',
        action='store_true',
        dest='upload',
        default=False,
        help='upload the package',
    )
    parser.add_argument(
        '--use-curl',
        action='store_true',
        dest='use_curl',
        default=False,
        help='use Curl when uploading the package',
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        dest='debug',
        default=False,
        help='emit debug info',
    )
    parser.add_argument(
        '--config',
        type=argparse.FileType('r'),
        dest='tomlfile',
        help='TOML config for the package',
    )

    args = parser.parse_args()

    conf = toml.load(args.tomlfile)

    pkg = conf['pkg']
    pkg_version = conf['pkg_version']
    pkg_zip = conf['pkg_zip']
    uploader = conf['uploader']
    email = conf['email']
    ctan_path = conf['ctan_path']

    toml_keynames = [
        'summary',
        'description',
        'author',
        'home',
        'announcement',
        'support',
        'development',
        'repository',
        'bugs',
        'note',
        'licenses',
        'topics',
        ]

    if args.use_curl:
        # certain parameters are always required
        curl = ['curl'] + [
            '-i',
            '-X',
            'POST',
            '-F',
            'update=true',
            '-F',
            'pkg=' + pkg,
            '-F',
            'version=' + pkg_version,
            '-F',
            'email=' + email,
            '-F',
            'ctanPath=' + ctan_path,
            '-F',
            'uploader=' + uploader,
            '-F',
            'file=@' + pkg_zip,
        ]

        for keyname in toml_keynames:
            add_curl_parm_from_toml(conf, keyname, curl)

        url = 'https://www.ctan.org/submit/validate'
        if args.upload:
            url = 'https://www.ctan.org/submit/upload'
        else:
            print('Validating upload')

        if args.debug:
            print(f'URL: {url}')
            print(f'Curl command: {curl}')
        curl.append(url)
        rc = subprocess.run(curl).returncode
        if rc:
            print(f'curl error: {rc}')
    else:
        payload = {
            'update': 'true',
            'pkg': pkg,
            'version': pkg_version,
            'email': email,
            'ctanPath': ctan_path,
            'uploader': uploader,
        }
        files = {'file': (pkg_zip, open(pkg_zip, 'rb'))}

        for keyname in toml_keynames:
            add_requests_parm_from_toml(conf, keyname, payload)

        url = 'https://www.ctan.org/submit/validate'
        if args.upload:
            url = 'https://www.ctan.org/submit/upload'
        else:
            print('Validating upload')

        if args.debug:
            print(f'URL: {url}')
            print(f'Payload: {payload}')
            print(f'Files: {files}')
        response = requests.post(
            url, data=payload, files=files
        )
        print(f'Response: ', response.status_code)
        if args.debug:
            for resp in response.headers:
                print(f'{resp:30}: {response.headers[resp]}')

        if response.text != '[]':
            print('Additional messages: ', response.text)


if __name__ == '__main__':
    main()
