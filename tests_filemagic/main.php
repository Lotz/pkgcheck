<?php
/***** Copyright 2012 Simon Dales
**
** This work may be distributed and/or modified under the
** conditions of the LaTeX Project Public License, either version 1.3
** of this license or (at your option) any later version.
** The latest version of this license is in
**   http://www.latex-project.org/lppl.txt
**
** This work has the LPPL maintenance status `maintained'.
** 
** The Current Maintainer of this work is Simon Dales.
*/

/*!
	\file
	\brief test code
	*/
	
/*!
	\mainpage
	
	Some test code.
	This shows a hierachy of classes.
	For this example we do animals.
	
	*/
	
require_once('animals.php');

// main
$animals = array(
	 new Cat
	,new Dog
	,new Bird
	,new RedKite
	,new Pigeon
	);
	
foreach ( $animals as $v)
	$v->call();

//eof
?>