# Development notes

## pkgcheck written in Rust

The predecessor tool was written in Perl and really slow when it had to check larger 
packages. 

I chose Rust for the programming language of the successor tool because on the one hand 
Rust is pretty fast, and on the other hand it gave me an opportunity to learn a new 
programming language. But don't assume I'm an Rust expert now.


## Dependencies

The `pkgcheck` binary is a 64-bit statically linked binary, and thus it should run also on 
older Linux versions.

It uses the following external programs:

- `pdfinfo` for checking pdf documents
- `unzip` for temporarily unpacking a TDS zip archive
- `chmod` for setting permissions (every Linux has it)

`pdfinfo` and `unzip` must be installed.


# Building the 64-bit static binary

The muslrust docker image must be gotten, see here: https://github.com/clux/muslrust
The a static binary could be build which gets linked against musl instead of libc.

The binary could be built like follows:
  `sudo docker run -v cargo-cache:/root/.cargo/registry:Z -v "$PWD:/volume:Z" --rm -it clux/muslrust:stable cargo build --release`

# How to build your own binary?

The following should work for macOS, Linux or other Unix-like systems.

## Install Rust

To install Rust just run
`curl https://sh.rustup.rs -sSf | sh` as shown here https://www.rust-lang.org/tools/install
and follow the on-screen instructions.

Then make sure the PATH contains `~/.cargo/bin`. This is the place where to find the Rust toolchain.

Make sure you have at least Rust 1.31.0 by checking: `rustc --version`

If you happen to have an older version simply run: `rustup update stable`

## Clone the git repository

`git clone https://gitlab.com/Lotz/pkgcheck.git`

## Build the binary

```
cd pkgcheck
cargo build --release
```

If all went well the binary is in: `target/release/pkgcheck`.



# Installation the binary

Copy the binary from `bin/pkgcheck` to a suitable location on your system, and 
(recommended) make sure the directory is in the `PATH`  or call `pkgcheck` using an 
absolute path name.


