// This file is generated by a Perl script. The source is
// in the docs/ directory of the repository.

use log::*;

pub fn e0001d() {
    error!(
        r#"
E0001 -- Bad characters in file name

File name should not contain non-ascii characters. Additionally, file
names should not contain control characters or other characters which
may have a special meaning for UNIX shells.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#nounixspecialcharacters
"#
    )
}

pub fn e0002d() {
    error!(
        r#"
E0002 -- File Permissions

Files submitted to CTAN should be world readable.

Only files that are truly executable (like scripts and binaries) should
be marked as such.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#filepermissions
"#
    )
}

pub fn e0003d() {
    error!(
        r#"
E0003 -- README is not a text file

The README file specified in the error message must be a text file but
it isn't.
"#
    )
}

pub fn e0004d() {
    error!(
        r#"
E0004 -- Empty directory not allowed

Empty directories are considered as rubbish, and are usually not
accepted as part of a package, neither in the package tree nor in the
TDS zip archive.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noemptyfiles
"#
    )
}

pub fn e0005d() {
    error!(
        r#"
E0005 -- Empty files not allowed

Empty files are considered as rubbish, and are usually not accepted as
part of a package.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noemptyfiles
"#
    )
}

pub fn e0006d() {
    error!(
        r#"
E0006 -- Hidden directories not allowed

A package should not contain hidden directories, neither in the package
tree nor in the TDS zip archive.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noauxfiles
"#
    )
}

pub fn e0007d() {
    error!(
        r#"
E0007 -- Hidden files not allowed

A package should not contain hidden files, neither in the package tree
nor in the TDS zip archive.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noauxfiles
"#
    )
}

pub fn e0008d() {
    error!(
        r#"
E0008 -- Temporary file detected

A temporary file was detected. These are typically files created by TeX
& friends and should not be part of a package.

Temporary files will also be detected in a TDS zip archive.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noauxfiles
"#
    )
}

pub fn e0009d() {
    error!(
        r#"
E0009 -- Package doesn't contain a README file

A package must contain at least one of README, README.md or README.txt
file.

For more details refer to:
http://mirrors.ibiblio.org/CTAN/help/ctan/CTAN-upload-addendum.html#readme
"#
    )
}

pub fn e0010d() {
    error!(
        r#"
E0010 -- Broken symlink detected

A broken symlink was detected.
"#
    )
}

pub fn e0011d() {
    error!(
        r#"
E0011 -- Wrong permission for directory

Directories should have rwx for the owner and at least r-x for others
(i.e. world readable).

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#filepermissions
"#
    )
}

pub fn e0012d() {
    error!(
        r#"
E0012 -- CRLF line endings detected

The file specified in the error message contains CRLF line endings. Text
files should have UNIX style line endings.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#crlf
"#
    )
}

pub fn e0013d() {
    error!(
        r#"
E0013 -- Socket special fie detected

The file specified in the error message is a socket special file which
is not allowed.
"#
    )
}

pub fn e0014d() {
    error!(
        r#"
E0014 -- Fifo special file detected

The file specified in the error message is a fifo special file which is
not allowed.
"#
    )
}

pub fn e0015d() {
    error!(
        r#"
E0015 -- Bloch device file detected

The file specified in the error message is a block device file which is
not allowed.
"#
    )
}

pub fn e0016d() {
    error!(
        r#"
E0016 -- Character device file detected

The file specified in the error message is a character device file which
is not allowed.
"#
    )
}

pub fn e0017d() {
    error!(
        r#"
E0017 -- PDF document is in error

The PDF document mentioned in the message is in error.

pdfinfo will be run to check if a PDF document can be read. Message
E0017 will be followed by the error messages from pdfinfo.

Example:

    I0002   Checking package files in directory somepkg
    E0017   PDF error detected in somepkg/sompkg.pdf
    Syntax Error (1293042): Illegal character ')'
    Syntax Error: Couldn't find trailer dictionary
    Syntax Error (1293042): Illegal character ')'
    Syntax Error: Couldn't find trailer dictionary
    Syntax Error: Couldn't read xref table
"#
    )
}

pub fn e0018d() {
    error!(
        r#"
E0018 -- Unwanted directory detected

A directory was detected which should not be part of a package. Example:
__MACOSX
"#
    )
}

pub fn e0019d() {
    error!(
        r#"
E0019 -- Generated file detected

In order to avoid redundancy we don't want to have included files in a
package which easily can be generated from other files in the
submission.

Exceptions are the README files of the package, i.e. README, README.md
or README.txt, .pdf, .html, or .css files.

pkgcheck detects generated files anywhere in the package directory tree.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#nogeneratedfiles
"#
    )
}

pub fn e0020d() {
    error!(
        r#"
E0020 -- Unwanted directory detected in the top level directory in TDS zip archive

The name of a top level directory of a TDS archive must be one of those
listed here: asymptote, bibtex, chktex, context, doc, dvipdfmx, dvips,
fonts, hbf2gf, makeindex, metafont, metapost, mft, omega, pbibtex,
psutils, scripts, source, tex, tex4ht, texconfig, texdoc, texdoctk,
ttf2pk, web2c, xdvi, xindy,

Any other other directory at the top level is an error.
"#
    )
}

pub fn e0021d() {
    error!(
        r#"
E0021 -- Error when reading a file

An error was encountered when reading the file specified in the message.
"#
    )
}

pub fn e0022d() {
    error!(
        r#"
E0022 -- Check of an URL in a README file failed

URL checking is in effect. An error occcurred when trying to retrieve an
URL which was found in the specified README file.
"#
    )
}

pub fn e0023d() {
    error!(
        r#"
E0023 -- Follow up error when trying to read a directory with insufficient permissions

Error which is a follow-up error. For instance, when a directory could
not be read.
"#
    )
}

pub fn e0024d() {
    error!(
        r#"
E0024 -- TDS zip archive has wrong permissions

The TDS zip archive should have at least r-- for the owner and at least
r-- for others (i.e. world readable).

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#filepermissions
"#
    )
}

pub fn e0025d() {
    error!(
        r#"
E0025 -- Duplicate names when ignoring letter case for files or directories

As there are operating systems which do not distinguish between myfile
and MYFILE we don't want to have file names in a directory which are the
same after converting to lower case.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#filenames
"#
    )
}

pub fn e0026d() {
    error!(
        r#"
E0026 -- Files not in TDS or different in TDS and non-install tree

The file mentioned in the error message is either not existing in the
TDS zip archive, or it is different to the one in the non-install tree
"#
    )
}

pub fn e0027d() {
    error!(
        r#"
E0027 -- An I/O error occurred

Some kind of I/O error occurred. If you believe there is an error in
pkgcheck please contact the author.
"#
    )
}

pub fn e0028d() {
    error!(
        r#"
E0028 -- A path name in a TDS zip archive must contain the package name

The path names in a TDS zip archive must contain the package name.

Example: Assume a package somepkg. Then path names should look like
follows:

    tex/latex/somepkg/somepkg.cls
    doc/latex/somepkg/README
    source/latex/somepkg/somepkg.dtx
    ...
"#
    )
}

pub fn e0029d() {
    error!(
        r#"
E0029 -- README file:  encoding with BOM detected

A README file should be either ASCII or UTF-8 without BOM(byte order
mark)

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#readme
"#
    )
}

pub fn e0030d() {
    error!(
        r#"
E0030 -- A symlink was found which points outside of the package directory tree

A symlink must not point to a file or directory outside of the package
directory tree.
"#
    )
}

pub fn e0031d() {
    error!(
        r#"
E0031 -- File name contains invalid UTF-8 character(s)

A file name contains invalid UTF-8 character(s).
"#
    )
}

pub fn e0033d() {
    error!(
        r#"
E0033 -- Error when unpacking tds archive

In order to investigate the contents of the TDS zip archive pkgcheck
unpacks the TDS zip archive to a temporary location which failed for the
reason given in the error message.
"#
    )
}

pub fn e0034d() {
    error!(
        r#"
E0034 -- Unwanted file detected in the top level directory in TDS zip archive

A top level directory of a TDS archive should only contain certain
directories but no files.
"#
    )
}

pub fn e0035d() {
    error!(
        r#"
E0035 -- Unwanted TDS archive detected in package directory tree

A package directory should not contain a TDS zip archive.
"#
    )
}

pub fn e0036d() {
    error!(
        r#"
E0036 -- .dtx/.ins files found in wrong directory in TDS zip archive

In a TDS zip archive a .dtx resp. .ins file must be in a subdirectory of
either of source/ or doc/ top level directories.
"#
    )
}

pub fn e0037d() {
    error!(
        r#"
E0037 -- CR line endings detected

The file specified in the error message contains CR line endings. Text
files should have UNIX style line endings.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#crlf
"#
    )
}

pub fn e0038d() {
    error!(
        r#"
E0038 -- File has inconsistent line endings: CR: x, LF: y, CRLF: z

The file specified in the error message contains CR line endings. Text
files should have UNIX style line endings.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#crlf
"#
    )
}

pub fn e0039d() {
    error!(
        r#"
E0039 -- No doc/ directory found in the top level directory of the TDS zip archive

A TDS zip archive is required to contain a top level directory doc/.
"#
    )
}

pub fn e0040d() {
    error!(
        r#"
E0040 -- Too few top level directories in the TDS zip archive

The top level directory of a TDS zip archive must contain at least a doc
directory and one or more of the following directories: asymptote,
bibtex, chktex, context, dvipdfmx, dvips, fonts, hbf2gf, makeindex,
metafont, metapost, mft, omega, pbibtex, psutils, scripts, source, tex,
tex4ht, texconfig, texdoc, texdoctk, ttf2pk, web2c, xdvi, xindy,

Any other other directory at the top level is an error.
"#
    )
}

pub fn e0041d() {
    error!(
        r#"
E0041 -- One or more map file found for the package but none of them is in a path starting with fonts/map/dvips

At least one map file was found which was not in a path starting with
fonts/map/dvips.
"#
    )
}

pub fn e0042d() {
    error!(
        r#"
E0042 -- TDS zip archive: duplicate names when ignoring letter case for files or directories

As there are operating systems which do not distinguish between myfile
and MYFILE we don't want to have file names in a directory which are the
same after converting to lower case.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#filenames
"#
    )
}

pub fn e0043d() {
    error!(
        r#"
E0043 -- Symlink  found in TDS zip archive

The TDS zip archive contained a symlink which is not allowed.
"#
    )
}

