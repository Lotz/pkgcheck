## E0037 -- CR line endings detected

The file specified in the error message contains CR line endings. Text files should
have UNIX style line endings. 


For more details refer to: 
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#crlf
