## E0023 -- Follow up error when trying to read a directory with insufficient permissions 

Error which is a follow-up error. For instance, when a directory could not be read.
