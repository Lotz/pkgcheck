#! /bin/sh

#
# get version identifier from Cargo.toml
#

version=$( perl -wln -e '/^version\s=\s"(.*)"/ and print $1;' ../Cargo.toml )


/bin/echo -E "\title{pkgcheck Utility, v$version}" > title.tex
