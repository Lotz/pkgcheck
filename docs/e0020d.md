## E0020 -- Unwanted directory detected in the top level directory in TDS zip archive

The name of a top level directory of a TDS archive must be one of those listed here:
`asymptote`, `bibtex`, `chktex`, `context`, `doc`, `dvipdfmx`, `dvips`, `fonts`, `hbf2gf`,
`makeindex`, `metafont`, `metapost`, `mft`, `omega`, `pbibtex`, `psutils`,
`scripts`, `source`, `tex`, `tex4ht`, `texconfig`, `texdoc`, `texdoctk`, `ttf2pk`,
`web2c`, `xdvi`, `xindy`,

Any other other directory at the top level is an error.
