## E0004 -- Empty directory not allowed 

Empty directories are considered as rubbish, and are usually not accepted as
part of a package, neither in the package tree nor in the TDS zip archive.

For more details refer to: 
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noemptyfiles
