#! /bin/sh

# create an empty config file and make it unreadable
touch     tests/f0009/bad_config.yml
chmod 111 tests/f0009/bad_config.yml

chmod 755 tests/f0009/somepkg
chmod 644 tests/f0009/somepkg/*
