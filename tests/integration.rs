// env LC_ALL=C sort tests/config_file/expected.data > tests/config_file/expected.data.sorted
#[cfg(test)]
mod integration {
    use std::process::Command;

    use assert_cmd::prelude::*;
    use predicates::prelude::*;

    use std::env;
    use std::fs;
    use std::fs::File;
    use std::io::prelude::*;
    use std::path::Path;

    fn sort_output_by_lines(output: &std::process::Output) -> String {
        let comb_output: Vec<u8> = output
            .stdout
            .clone()
            .into_iter()
            .chain(output.stderr.clone().into_iter())
            .collect();
        let s = String::from_utf8_lossy(&comb_output);

        let mut lvec = Vec::new();
        let lines = s.split('\n');

        for l in lines {
            lvec.push(l);
        }
        lvec.sort_unstable();
        let lvec1 = lvec.iter().map(|v| format!("{}\n", v)).collect::<String>();

        // skip the last newline
        lvec1[1..lvec1.len()].to_string()
    }

    // returns true if file is a regular file and does exist
    // returns false otherwise
    fn exists_file(file: &str) -> bool {
        match fs::metadata(file) {
            Ok(attr) => attr.is_file(),
            Err(_) => false,
        }
    }

    fn run_script(script: &str) {
        let _ = Command::new("sh")
            .arg("-c")
            .arg(script)
            .output()
            .expect("failed to execute process");
    }

    fn file_contents(file: &str) -> String {
        let fhdl = File::open(&file);
        match fhdl {
            Ok(mut f) => {
                let mut buf = String::new();
                match f.read_to_string(&mut buf) {
                    Ok(_bytes_read) => {
                        return buf;
                    }
                    Err(e) => panic!("Error {:?} reading file {}", file, e),
                }
            }
            Err(e) => panic!("Error reading file {}: {:?}", file, e),
        }
    }

    fn main_binary() -> String {
        let runner = ::escargot::CargoBuild::new()
            .current_release()
            .run()
            .unwrap();
        runner.path().display().to_string()
    }

    macro_rules! error_tests {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        match e {
                            "e0008_1" |  "e0020" | "e0024" | "e0025_42_43" | "e0026" | "e0028" | "e0033"
                                | "e0034" | "e0036" | "e0039" | "e0040" | "e0040a" =>
                                vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ],
                            "e0022" | "e0037" | "e0038" | "w0008" =>
                                vec!["--no-colors", "--urlcheck", "-d", pkg_dir ],
                            "w0009" =>
                                vec!["--no-colors",  "--verbose", "--config-file", "tests/w0009/pkgcheck.yml", "-d", pkg_dir ],
                            _ =>
                                vec!["--no-colors", "-d", pkg_dir]
                        };

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_output_by_lines(&output);


                    assert_eq!(&sorted, &file_contents(expected_data_file));
                    assert!(!output.status.success());

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }
                }
            )*
        }
    }

    error_tests! {
        e0001,
        e0002,
        // BUGBUG seems to be rubbish e0002a,
        e0003,
        e0004,
        e0005,
        e0006,
        e0007,
        w0008,
        e0008,
        e0008_1,
        e0009,
        e0010,
        e0011,
        e0012,
        e0013,
        e0014,
        // e0015: we skip that test because we need root to create a block device file
        // e0016: we skip that test because we need root to create a character device file
        e0017,
        e0018,
        e0019,
        e0019_1,
        e0019_2,
        e0019_3,
        e0020,
        e0021,
        e0022,
        e0023,
        e0024,
        e0025_42_43,
        e0025a,
        e0026,
        //e0027, // some kind of I/O error. Don't know how to create a test case
        e0028,
        e0029,
        e0029_1,
        e0030,
        e0030_1,
        e0034,
        e0033,
        e0035,
        e0036,
        e0037,
        e0038,
        e0039,
        e0040,
        e0040a,
        w0009,
    }

    macro_rules! ok_tests {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    // let e = stringify!($name);
                    // let pkg_dir = &format!("tests/{}/somepkg", e);
                    // let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    // let expected_data = &format!("tests/{}/expected.data", e);
                    // let arguments =
                    //     if e == "ok002" {
                    //         vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ]
                    //     } else {
                    //         vec!["--no-colors", "-d", pkg_dir]
                    //     };

                    // Command::main_binary()
                    //     .expect("singe-binary package that compiles")
                    //     .args(&arguments)
                    //     .assert()
                    //     .success()   // we have always success
                    //     .stdout(predicate::path::eq_file(Path::new(expected_data)));


                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        match e {
                            "ok001_1" =>
                            {
                                let tdir = Path::new("tests/ok001_1/somepkg");
                                assert!(env::set_current_dir(&tdir).is_ok());
                                vec!["--no-colors", "-d", "./"]
                            },
                            "ok002" | "w0006" => vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ],
                            _ => vec!["--no-colors", "-d", pkg_dir],
                    };


                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }
                    println!("{:?}", &arguments);
                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    if e == "ok001_1" {
                        let tdir = Path::new("../../..");
                        assert!(env::set_current_dir(&tdir).is_ok());
                    }

                    let sorted = sort_output_by_lines(&output);

                    assert_eq!(&sorted, &file_contents(expected_data_file));
  //???                  assert!(output.status.success());

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }

                }
            )*
        }
    }

    ok_tests! {
            ok001,
    // ???        ok001_1,  // with `cd tests/ok001_1`
            ok002,
            w0001,
            w0002,
            w0004,
            w0005,
            w0006,
        }

    #[test]
    fn pkg004_perm_toplevel_dir_e0011_e0023_e0009() {
        // Command::main_binary()
        //     .expect("singe-binary package that compiles")
        //     .args(&["--no-colors", "-d", "tests/pkg004/somepkg" ])
        //     .assert()
        //     .code(1) // or .success()
        //     .stdout(predicate::path::eq_file(Path::new("tests/pkg004/expected.data.sorted")));

        let e = "pkg004";
        let expected_data_file = &format!("tests/{}/expected.data.sorted", e);

        let before = &format!("tests/{}/before.sh", e);
        if exists_file(before) {
            run_script(before);
        }

        let output = Command::new(main_binary())
            .args(&["--no-colors", "-d", "tests/pkg004/somepkg"])
            .output()
            .expect("failed to execute process");

        let sorted = sort_output_by_lines(&output);

        assert_eq!(&sorted, &file_contents(expected_data_file));

        let after = &format!("tests/{}/after.sh", e);
        if exists_file(after) {
            run_script(after);
        }
    }

    #[test]
    fn f0002() {
        Command::cargo_bin("pkgcheck")
            .expect("singe-binary package that compiles")
            .args(&["--no-colors", "-d", "tests/not_existing/somepkg"])
            .assert()
            .code(1)
            .stderr(predicate::path::eq_file(Path::new(
                "tests/f0002/expected.data",
            )));
    }

    // #[test]
    // fn tds_errors() {
    //     for (dir, pkg, tds_zip) in
    //         vec![ ("tests/tds_not_existing", "somepkg", "somepkg.tds.zip"),
    //                 ( "tests/tds_wrong_name" , "somepkg", "somepkg.zip")] {
    //             let d_parm = &format!("{}/{}", dir, pkg );
    //             let tds_parm = &format!("{}/{}", dir, tds_zip);
    //             let expected_data = &format!("{}/expected.data.sorted", dir);

    //             let before = &format!("{}/before.sh", dir);
    //             if exists_file(dir) {
    //                 run_script(before);
    //             }

    //             Command::main_binary()
    //                 .expect("singe-binary package that compiles")
    //                 .args(&["--no-colors", "-d", d_parm, "-T", tds_parm ])
    //                 .assert()
    //                 .code(1)
    //                 .stdout(predicate::path::eq_file(Path::new(expected_data)));

    //             let after = &format!("{}/after.sh", dir);
    //             if exists_file(after) {
    //                 run_script(after);
    //             }
    //         }
    // }

    macro_rules! config_file_errors {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);

                    let arguments =
                        match e {
                            "f0008" => vec! [ "-d", "tests/f0008/somepkg", "--config-file", "tests/f0008/pkgcheck.yml"],
                            "f0009" => vec! [ "-d", "tests/f0009/somepkg", "--config-file", "tests/f0009/bad_config.yml"],
                            "f0010" => vec! [ "-d", "tests/f0009/somepkg", "--config-file", "tests/f0010/bad_config.yml"],
                            // "f0004" => vec! [ "-d", "tests/f0004/somepkg", "-T", "tests/f0004/somepkg.tds.zip"],
                            // "f0005" => vec! [ "-d", "tests/f0005/somepkg", "-T", "tests/f0005/somepkg.zip"],
                            // "f0007" => vec! [ "-d", "tests/f0007/somepkg", "-T", "tests/f0007/somepkg.tds.zip"],
                            _ => panic!("undefined testcase"),
                        };
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output =
                        Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");


                    let sorted = sort_output_by_lines(&output);


                    assert!(!output.status.success());

                    // If the error is "f0007" we have to consider that the temporary directory
                    // name ends in random characters. So, pragmatically we check only for the
                    // first 175 characters which are equal
                    if e == "f0007" {
                        let equal_len = 175;
                        assert_eq!(&sorted[..equal_len], &file_contents(expected_data_file)[..equal_len]);
                    } else {
                        assert_eq!(&sorted, &file_contents(expected_data_file));
                    }

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }

                }
            )*
        }
    }
    config_file_errors! {
        f0008,                  // config_file not existing,
        f0009,                  // error reading config file,
        f0010,                  // config file with bad content
    }

    macro_rules! tds_errors {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    // let (dir, pkg, tds_zip) =
                    //     match stringify!($name) {
                    //         "f0003" => ( "tests/f0003", "somepkg", "somepkg.tds.zip"),
                    //         "f0004" => ( "tests/f0004", "somepkg", "somepkg.tds.zip"),
                    //         "f0005" => ( "tests/f0005", "somepkg", "somepkg.zip"),
                    //         "f0007" => ( "tests/f0007", "somepkg", "somepkg.zip"),
                    //         _ => panic!("undefined testcase"),
                    //     };

                    let arguments =
                        match e {
                            "f0003" => vec! [ "-d", "tests/f0003/somepkg", "-T", "tests/f0003/somepkg.tds.zip"],
                            "f0004" => vec! [ "-d", "tests/f0004/somepkg", "-T", "tests/f0004/somepkg.tds.zip"],
                            "f0005" => vec! [ "-d", "tests/f0005/somepkg", "-T", "tests/f0005/somepkg.zip"],
                            "f0007" => vec! [ "-d", "tests/f0007/somepkg", "-T", "tests/f0007/somepkg.tds.zip"],
                            _ => panic!("undefined testcase"),
                        };
                    //let d_parm = &format!("{}/{}", dir, pkg );
                    //let tds_parm = &format!("{}/{}", dir, tds_zip);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output =
                        if e == "f0007" {
                        Command::new(main_binary())
                        .env("TMPDIR", "tests/f0007/tmpdir")
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process")

                        } else {
                        Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process")

                        };


                    let sorted = sort_output_by_lines(&output);


                    assert!(!output.status.success());

                    // If the error is "f0007" we have to consider that the temporary directory
                    // name ends in random characters. So, pragmatically we check only for the
                    // first 175 characters which are equal
                    if e == "f0007" {
                        let equal_len = 175;
                        assert_eq!(&sorted[..equal_len], &file_contents(expected_data_file)[..equal_len]);
                    } else {
                        assert_eq!(&sorted, &file_contents(expected_data_file));
                    }

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }

                }
            )*
        }
    }
    tds_errors! {
        f0003,                  // tds_not_existing,
        f0004,                  // tds zip archive is no zip file
        f0005,                  // tds_wrong_name,
        f0007,
    }

    #[test]
    fn f0001() {
        Command::cargo_bin("pkgcheck")
            .expect("singe-binary package that compiles")
            .args(&["--no-colors"])
            .assert()
            .code(1)
            .stderr(predicate::path::eq_file(Path::new(
                "tests/f0001/expected.data",
            )));
    }

    #[test]
    fn calling_explain() {
        Command::cargo_bin("pkgcheck")
            .expect("singe-binary package that compiles")
            .args(&["--no-colors", "--explain", "i0001"])
            .assert()
            .success()
            .stdout(predicate::path::eq_file(Path::new(
                "tests/explain_i0001/expected.data",
            )));
    }

    macro_rules! correct_perms {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        if e == "ok002" {
                            vec!["--no-colors", "-C", "-d", pkg_dir, "-T", tds_zip_archive ]
                        } else {
                            vec!["--no-colors", "-C", "-d", pkg_dir]
                        };

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_output_by_lines(&output);


                    assert_eq!(&sorted, &file_contents(expected_data_file));

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }
                }
            )*
        }
    }

    correct_perms! {
        cp_001,
    }

    macro_rules! chdir_tests {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        match e {
                            "ok001_1" =>
                                vec!["--no-colors", "-d", "./"],
                            "ok001_2" =>
                                vec!["--no-colors", "-d", "."],
                            _ => vec!["--no-colors", "-d", pkg_dir],
                    };


                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let tdir = Path::new("tests/ok001_1/somepkg");
                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .current_dir(&tdir)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_output_by_lines(&output);

                    assert_eq!(&sorted, &file_contents(expected_data_file));
                    assert!(output.status.success());

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }

                }
            )*
        }
    }

    chdir_tests! {
        ok001_1,  // with `cd tests/ok001_1` and `-d ./`
        ok001_2,  // with `cd tests/ok001_1` and `-d .`
    }
}
