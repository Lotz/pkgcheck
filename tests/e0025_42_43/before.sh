#! /bin/sh
set -x
find tests/e0025_42/ -type d -print0 | xargs -0 chmod 755
find tests/e0025_42/ -type f -print0 | xargs -0 chmod 644
rm tests/e0025_42/somepkg.tds.zip
cd tests/e0025_42/tds && zip --symlinks -r ../somepkg.tds.zip .
