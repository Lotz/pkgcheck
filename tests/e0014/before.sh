#! /bin/sh

chmod 755 tests/e0014/somepkg/
chmod 644 tests/e0014/somepkg/README
chmod 644 tests/e0014/somepkg/xyz.tex
myfifo='tests/e0014/somepkg/somefifo'
[[ -e $myfifo ]] || mkfifo $myfifo
chmod 644 $myfifo
