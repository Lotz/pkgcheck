 _____________________________________________
/ This repository has been moved to codeberg. \
\ https://codeberg.org/ManfredLotz/pkgcheck   /
 ---------------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||


---
pkgcheck utility

Author: Manfred Lotz, <manfred@ctan.org>

License: Apache License, Version 2.0 or MIT License

Copyright (c) 2018 Manfred Lotz
---


# Overview

`pkgcheck` is a utility which the author uses to check uploaded packages to CTAN before
installing them. It is a binary running on Linux only. 

There is no Windows version planned.


# Dependencies

The `pkgcheck` binary is a 64-bit statically linked binary, and thus it should run also on 
older Linux versions.

It uses `pdfinfo` for checking pdf documents.


# Installing the binary

Copy the binary from `bin/pkgcheck` to a suitable location on your hard disk, and 
(recommended) make sure the directory is in the `PATH` or call `pkgcheck` using an 
absolute path name.


# Documentation

The documentation is `docs/pkgcheck.pdf`. It contains 
a description of all fatal, error, warning and information messages.


# Build the documentation

Run either `xelatex` or `lualatex`. Note that `-shell-escape` is required.


```
cd docs 
lualatex -shell-escape pkgcheck.tex
```

# License 

Licensed under either of

-  Apache License, Version 2.0

   - See file LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0

-  MIT license

   - See file LICENSE-MIT or http://opensource.org/licenses/MIT

at your option.

